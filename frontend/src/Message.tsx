
export interface Message {
  ts: string;
  message: string;
  to: string;
  from: string;
}
