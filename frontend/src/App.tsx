import { useEffect, useState } from 'react';
import { MainScreen } from './screens/MainScreen';
import { SignInScreen } from './screens/SignInScreen';
import { Message } from './Message';

type MessageCollection = Record<string, Message[]>;

function App() {
  const [websocket, setWebSocket] = useState<WebSocket | null>(null);
  const [messages, setMessages] = useState<MessageCollection>({})
  const [signedInAs, setSignedInAs] = useState<string | null>(null);
  const [selectedUser, setSelectedUser] = useState<string | null>(null);

  useEffect(() => {
    const ws = new WebSocket("ws://localhost:5678/");
    setWebSocket(ws);

    ws.onopen = () => {
      console.log("Connected to WebSocket");
    };

    ws.onclose = function() {
      console.log("Disconnected from WebSocket");
    };

    ws.onerror = (error) => {
      console.error("WebSocket error", error, event);
    };

    return () => {
      ws.close();
    };
  }, []);

  function handleNewUserRegistration(screenName: string): (collection: MessageCollection) => MessageCollection {
    return (collection: MessageCollection) => {
      return {
        ...collection, 
        [screenName]: []
      }
    }
  }

  function handleNewMessage(message: Message): (collection: MessageCollection) => MessageCollection {
    return (collection: MessageCollection) => {
      const key = message.from === signedInAs ? message.to : message.from;
      return {
        ...collection,
        [key]: [
          ...collection[key],
          message
        ]
      }
    }
  }

  useEffect(() => {
    if (!websocket || websocket.readyState !== WebSocket.OPEN) {
      return;
    }
    websocket.onmessage = (event) => {
      const { data } = event;
      const json = JSON.parse(data);
      const payload = json["data"]
      if (json["type"] == "register") {
        setMessages(handleNewUserRegistration(payload["screen_name"]))
      }
      if (json["type"] == "new_message") {
        setMessages(handleNewMessage(payload as Message))
      }
    };
  }, [signedInAs, websocket?.readyState])

  function onJoin(s: string) {
    if (!websocket || websocket.readyState !== WebSocket.OPEN) {
      console.error("attempted to sign in without valid ws connection")
      return;
    }
    const message = JSON.stringify({
      type: "register",
      data: { "screenName": s }
    })
    websocket.send(message);
    setSignedInAs(s);
  }

  function onMessageSend(s: string) {
    if (!websocket || websocket.readyState !== WebSocket.OPEN) {
      console.error("attempted to sign in without valid ws connection")
      return;
    }
    websocket.send(JSON.stringify({
      type: "message",
      data: {
        from: signedInAs,
        message: s,
        to: selectedUser
      }
    }))
  }

  function onUserSelect(s: string) {
    setSelectedUser(s);
  }

  return (
    <>
      {signedInAs === null && <SignInScreen onJoin={onJoin} />}
      {signedInAs !== null && (
        <MainScreen
          onMessageSend={onMessageSend}
          name={signedInAs}
          users={Object.keys(messages)}
          onUserClick={onUserSelect}
          selectedUser={selectedUser}
          messagaes={selectedUser !== null ? messages[selectedUser] : []}
        />
      )}
    </>
  );
}

export default App;

