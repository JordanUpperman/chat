export function getImageUrlForUser(s: string): string {
  return `https://api.dicebear.com/8.x/notionists-neutral/svg?seed=${s}`
}