import { useState } from "react"

interface SignInProps {
  onJoin: (name: string) => void
}

export function SignInForm(props: SignInProps) {
  const [name, setName] = useState<string>("")
  function onNameChange(e: React.ChangeEvent<HTMLInputElement>) {
    setName(e.target.value);
  }
  return (
    <div className="ring-1 ring-white rounded-lg bg-gray-800 flex min-h-full flex-col justify-center px-6 py-12 lg:px-8">
      <div className="sm:mx-auto sm:w-full sm:max-w-sm">
        <div className="mb-5">
          <label htmlFor="screenName" className="block text-lg font-bold leading-6">
            Enter Screen Name
          </label>
          <div className="mt-2 mb-3">
            <input 
              id="screenName"
              name="screenName" 
              type="text" 
              required 
              className="block w-full rounded-md border-0 px-2 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" 
              value={name}
              onChange={onNameChange}
            />
          </div>
        </div>
        <div>
          <button
            type="submit"
            className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            onClick={() => props.onJoin(name) }
          >
            Join Chat
          </button>
        </div>
      </div>
    </div>
  )
}