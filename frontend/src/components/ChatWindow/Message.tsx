export function Message(props: { fromUs: boolean; text: string; }) {
  const outerClass = props.fromUs ? "justify-end" : "justify-start";
  const innerClass = props.fromUs ? "bg-green-600" : "bg-blue-600";

  return <div data-testid="message-outer-container" className={`flex ${outerClass}`}>
    <div data-testid="message-inner-container" className={`my-2 mx-3 rounded-md ${innerClass}`}>
      <span data-testid="message-text" className='p-3 text-black'>{props.text}</span>
    </div>
  </div>;
}
