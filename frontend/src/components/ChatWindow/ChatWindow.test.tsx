import {  fireEvent, render, screen } from "@testing-library/react"
import { ChatWindow } from "./ChatWindow"
import '@testing-library/jest-dom'

const otherUser = "otherUser";
const fromOther = "messageFromThem";
const activeUser = "activeUser";
const fromActive = "messageFromActive";
const otherFromActive = "otherMessageFromActive";
const textToSend = "textToSend";
const now = Date.now();
const onSend = jest.fn()

describe('ChatWindow', () => {
    beforeEach(() => {
        render(<ChatWindow
            name={otherUser}
            onSend={onSend}
            messagaes={[
                {
                    to: activeUser,
                    from: otherUser,
                    message: fromOther,
                    ts: new Date(now - 100).toISOString()
                },
                {
                    to: otherUser,
                    from: activeUser,
                    message: fromActive,
                    ts: new Date(now - 1000).toISOString()
                },
                {
                    to: otherUser,
                    from: activeUser,
                    message: otherFromActive,
                    ts: new Date(now).toISOString()
                }
            ]}
        />)
    });

    it('calls onSend method with text in input when button is clicked', () => {
        const input = screen.getByTestId('chat-input');
        const button = screen.getByTestId('send-chat-button');

        fireEvent.change(input, { target:{ value: textToSend}})
        fireEvent.click(button);

        expect(onSend).toHaveBeenCalledWith(textToSend);
    });
    
    it('renders messages sorted oldest first', () => {
        const first = screen.getByText(fromActive)
        const second = screen.getByText(fromOther)
        const third = screen.getByText(otherFromActive)

        expect(first.compareDocumentPosition(second)).toEqual(4);
        expect(second.compareDocumentPosition(third)).toEqual(4);
    });

    it('renders messages from active user as green', () => {
        const firstFromActive = screen.getByText(fromActive)
        const secondFromActive = screen.getByText(otherFromActive)

        expect(firstFromActive.parentElement).toHaveClass("bg-green-600");
        expect(secondFromActive.parentElement).toHaveClass("bg-green-600");
    });

    it('renders messages from active user end justified', () => {
        const firstFromActive = screen.getByText(fromActive).parentElement
        const secondFromActive = screen.getByText(otherFromActive).parentElement

        expect(firstFromActive?.parentElement).toHaveClass("justify-end");
        expect(secondFromActive?.parentElement).toHaveClass("justify-end");
    });

    it('renders messages from other user as blue', () => {
        const fromOtherParent = screen.getByText(fromOther)

        expect(fromOtherParent.parentElement).toHaveClass("bg-blue-600");
    });

    it('renders messages from other user start justified', () => {
        const fromOtherParent = screen.getByText(fromOther).parentElement

        expect(fromOtherParent?.parentElement).toHaveClass("justify-start");
    });
})