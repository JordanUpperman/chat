import { MessageHeader } from './MessageHeader';
import { MessageInput } from './MessageInput';
import { MessageFeed } from './MessageFeed';
import { Message } from '../../Message';

interface ChatWindowProps {
  name: string
  onSend: (message: string) => void
  messagaes: Message[]
} 

export function ChatWindow(props: ChatWindowProps) {
  const sortedMessages = props.messagaes.sort((m1, m2) => {
    return new Date(m1.ts).getTime() - new Date(m2.ts).getTime()
  })
  return <div className="w-full h-full h-100 bg-white flex flex-col">
    <MessageHeader name={props.name}/>
    <MessageFeed messagaes={sortedMessages} target={props.name} />
    <MessageInput onSend={props.onSend}/>
  </div>;
}
