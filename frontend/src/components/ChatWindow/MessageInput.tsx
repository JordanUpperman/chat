import { useState } from "react";

interface MessageInputProps {
  onSend: (message: string) => void}

export function MessageInput(props: MessageInputProps) {
  const [message, setMessage] = useState('');
  function onValueChange(e: React.ChangeEvent<HTMLInputElement>) {
    setMessage(e.target.value)
  }
  async function onClick() {
    props.onSend(message)
    setMessage('')
  }

  return <div className='min-h-8 flex flex-row pt-1 bg-black'>
    <input
      placeholder="Type a message to send..."
      type="text" 
      className='p-2 bg-transparent flex-grow accent-slate-600'
      data-testid="chat-input"
      onChange={onValueChange}
    />
    <button
      data-testid="send-chat-button"
      className="flex justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-7 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
      onClick={onClick}
    >Send</button>
  </div>;
}
