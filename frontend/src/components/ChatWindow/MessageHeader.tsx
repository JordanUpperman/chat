import { getImageUrlForUser } from '../../utils';

interface MessageHeaderProps {
  name: string;
}
export function MessageHeader(props: MessageHeaderProps) {
  return <div className='w-full bg-indigo-800 flex flex-row align-baseline justify-between'>
    <img alt={props.name} className='w-8 h-8 m-2 rounded-full' src={getImageUrlForUser(props.name)} />
    <div className='self-center'>
      <span className='align-baseline mr-2 font-semibold text-lg'>{props.name}</span>
    </div>
  </div>;
}
