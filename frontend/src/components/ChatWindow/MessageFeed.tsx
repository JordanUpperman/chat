import { Message as MessageType } from '../../Message';
import { Message } from './Message';

interface MessageFeedProps {
  messagaes: MessageType[],
  target: string
}
export function MessageFeed(props: MessageFeedProps) {
  return <div className='overflow-auto flex-grow bg-gray-600 flex flex-col'>
    { props.messagaes.map(m => <Message 
          fromUs={m.from !== props.target}
          text={m.message}
          key={m.ts}
          />
      )}
  </div>;
}
