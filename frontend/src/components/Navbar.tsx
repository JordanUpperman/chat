import { getImageUrlForUser } from "../utils";

export function Navbar(props: {name: string}) {
  return (
  <nav className="w-100 bg-gray-800 sticky top-0">
  <div className="mx-auto w-100 px-2 sm:px-6 lg:px-8">
    <div className="relative flex h-16 items-center justify-end">

      <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
          <span>
            {props.name}
          </span>
        <div className="relative ml-3">
          <div>
              <img className="h-8 w-8 rounded-full" src={getImageUrlForUser(props.name)} alt=""/>
          </div>
        </div>
      </div>
    </div>
  </div>
</nav>
)
}