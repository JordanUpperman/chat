import { getImageUrlForUser } from '../../utils';

export function UserPreview(props: { user: string; onClick: (s: string) => void; }) {
  return <div className="flex align-center" onClick={() => props.onClick(props.user)}>
    <img className="h-7 w-7 rounded-full" src={getImageUrlForUser(props.user)} alt={props.user} />
    <span className='ml-2'>{props.user}</span>
  </div>;
}
