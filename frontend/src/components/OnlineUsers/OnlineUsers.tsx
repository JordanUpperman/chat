import { UserPreview } from './UserPreview';

export function OnlineUsers(props: { users: string[]; onUserClick: (s: string) => void; }) {
  return <ul>
    {props.users.map(u => <li className='border-1 border-white'>
      <UserPreview user={u} onClick={props.onUserClick} />
    </li>
    )}
  </ul>;
}
