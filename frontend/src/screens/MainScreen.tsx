import { Message } from '../Message';
import { ChatWindow } from '../components/ChatWindow';
import { OnlineUsers } from '../components/OnlineUsers';
import { Navbar } from '../components/Navbar';

interface MainScreenProps {
  name: string;
  users: string[];
  onUserClick: (s: string) => void;
  selectedUser: string | null;
  onMessageSend: (s: string) => void;
  messagaes: Message[]
}

export function MainScreen(props: MainScreenProps) {
  return <>
    <div className="grid h-screen grid-cols-5 grid-rows-3 gap-5">
      <div className="col-span-5">
        <Navbar name={props.name} />
      </div>
      <div className="col-span-2 row-span-2 col-start-3">
        {props.selectedUser !== null && <ChatWindow  
          name={props.selectedUser} 
          onSend={props.onMessageSend}
          messagaes={props.messagaes}
        />}
      </div>
      <div className="col-span-1 col-start-5">
        <OnlineUsers users={props.users} onUserClick={props.onUserClick} />
      </div>
    </div>
  </>;
}
