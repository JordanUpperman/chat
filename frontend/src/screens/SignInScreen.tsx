import { SignInForm } from '../components/SignInForm';

export function SignInScreen(props: { onJoin: (s: string) => void; }) {
  return <div className="grid grid-cols-5 grid-rows-3 gap-5">
    <div className="col-start-2 row-start-2 col-span-3">
      <SignInForm onJoin={props.onJoin} />
    </div>
  </div>;
}
