import unittest
from unittest.mock import Mock, create_autospec
import unittest.async_case
from package.user_collection import UserCollection
from package.registration_handler import RegistrationHandler
from websockets.legacy.server import WebSocketServerProtocol
from typing import cast, Callable
import json

class TestRegistrationHandler(unittest.async_case.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self.__collection_mock = cast(UserCollection, Mock(spec=UserCollection))
        self.__handler = RegistrationHandler(self.__collection_mock)
        return super().setUp()
    
    async def test_handle__appends_user_to_collection(self):
        append_user_mock: Mock = self.__collection_mock.append_user
        socket = Mock(spec=WebSocketServerProtocol)
        user = "fake_user"

        await self.__handler.handle(user, socket)

        append_user_mock.assert_called_once_with(user, socket)
    
    async def test_handle__calls_for_every(self):
        for_every_mock: Mock = self.__collection_mock.for_every_async
        socket = Mock(spec=WebSocketServerProtocol)
        user = "fake_user"

        await self.__handler.handle(user, socket)

        for_every_mock.assert_called_once()
    
    async def test_hadle__for_every_callback_calls_send_on_both_websockets(self):
        for_every_mock: Mock = self.__collection_mock.for_every_async
        
        socket_1 = cast(WebSocketServerProtocol, Mock(spec=WebSocketServerProtocol))
        send_1: Mock = socket_1.send
        user_1 = "fake_user"
        
        socket_2 = cast(WebSocketServerProtocol, Mock(spec=WebSocketServerProtocol))
        send_2: Mock = socket_2.send
        user_2 = "fake_user_2"

        await self.__handler.handle(user_1, socket_1)
        callback: Callable[[str, WebSocketServerProtocol], None] = for_every_mock.call_args[0][0]
        await callback(user_2, socket_2)

        send_1.assert_called_once()
        send_2.assert_called_once()

    async def test_hadle__for_every_callback_calls_send_with_correct_arguments(self):
        for_every_mock: Mock = self.__collection_mock.for_every_async
        
        socket_1 = cast(WebSocketServerProtocol, Mock(spec=WebSocketServerProtocol))
        send_1: Mock = socket_1.send
        user_1 = "fake_user"
        
        socket_2 = cast(WebSocketServerProtocol, Mock(spec=WebSocketServerProtocol))
        send_2: Mock = socket_2.send
        user_2 = "fake_user_2"

        await self.__handler.handle(user_1, socket_1)
        callback: Callable[[str, WebSocketServerProtocol], None] = for_every_mock.call_args[0][0]
        await callback(user_2, socket_2)

        send_1.assert_called_with(json.dumps({"type": "register", "data": {"screen_name": user_2}}))
        send_2.assert_called_with(json.dumps({"type": "register", "data": {"screen_name": user_1}}))


