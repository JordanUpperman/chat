import unittest
import unittest.async_case
from unittest.mock import Mock, AsyncMock
from websockets.legacy.server import WebSocketServerProtocol

from package.user_collection import (
    UserCollection,
    UserAlreadyExistsException,
    UserEmptyException,
    UserDoesNotExistException,
)


class UserCollectionTests(unittest.async_case.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self._collection = UserCollection()
        return super().setUp()

    def test_append_user__throws_error_if_user_already_present(self):
        user = "test_user"
        socket = Mock(spec=WebSocketServerProtocol)
        self._collection.append_user(user, socket)

        with self.assertRaises(UserAlreadyExistsException):
            self._collection.append_user(user, socket)

    def test_append_user__throws_error_if_user_is_empty(self):
        user = ""
        socket = Mock(spec=WebSocketServerProtocol)

        with self.assertRaises(UserEmptyException):
            self._collection.append_user(user, socket)

    def test_get_socket_for_user__throws_if_not_present(self):
        user = "test_user"

        with self.assertRaises(UserDoesNotExistException):
            self._collection.get_socket_for_user(user)

    def test_get_socket_for_user__returns_socket_if_present(self):
        socket = Mock(spec=WebSocketServerProtocol)
        user = "test_user"
        self._collection.append_user(user, socket)

        found = self._collection.get_socket_for_user(user)

        self.assertIs(found, socket)

    def test_get_user_for_socket__returns_correct_user(self):
        socket_1 = Mock(spec=WebSocketServerProtocol)
        user_1 = "test_user"
        self._collection.append_user(user_1, socket_1)

        socket_2 = Mock(spec=WebSocketServerProtocol)
        user_2 = "test_user_2"
        self._collection.append_user(user_2, socket_2)

        found = self._collection.get_user_for_socket(socket_1)

        self.assertIs(found, user_1)

    def test_get_user_for_socket__raises_exception_if_user_not_found(self):
        socket_1 = Mock(spec=WebSocketServerProtocol)
        user_1 = "test_user"
        self._collection.append_user(user_1, socket_1)

        socket_2 = Mock(spec=WebSocketServerProtocol)

        with self.assertRaises(UserDoesNotExistException):
            self._collection.get_user_for_socket(socket_2)

    def test_remove_user__does_not_throw_if_not_present(self):
        self._collection.remove_user("test_user")

    def test_remove_user__removes_user_if_present(self):
        user = "test_user"
        socket = Mock(spec=WebSocketServerProtocol)
        self._collection.append_user(user, socket)
        self._collection.remove_user(user)

        with self.assertRaises(UserDoesNotExistException):
            self._collection.get_user_for_socket(socket)

        with self.assertRaises(UserDoesNotExistException):
            self._collection.get_socket_for_user(user)

    async def test_for_every_async__calls_method_for_all_users(self):
        mock_method = AsyncMock()
        socket_1 = Mock(spec=WebSocketServerProtocol)
        user_1 = "test_user"
        self._collection.append_user(user_1, socket_1)

        socket_2 = Mock(spec=WebSocketServerProtocol)
        user_2 = "test_user_2"
        self._collection.append_user(user_2, socket_2)

        async def fake_callback(user: str, socket: WebSocketServerProtocol):
            await mock_method(user, socket)

        await self._collection.for_every_async(fake_callback)

        mock_method.assert_any_call(user_1, socket_1)
        mock_method.assert_any_call(user_2, socket_2)

    async def test_for_every_async__when_one_raises_exception_still_calls_other(self):
        mock_method = AsyncMock()
        socket_1 = Mock(spec=WebSocketServerProtocol)
        user_1 = "test_user"
        self._collection.append_user(user_1, socket_1)

        socket_2 = Mock(spec=WebSocketServerProtocol)
        user_2 = "test_user_2"
        self._collection.append_user(user_2, socket_2)

        async def fake_callback(user: str, socket: WebSocketServerProtocol):
            if user == user_1:
                raise Exception("not this one")
            await mock_method(user, socket)

        await self._collection.for_every_async(fake_callback)

        mock_method.assert_any_call(user_2, socket_2)
