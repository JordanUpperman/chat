#!/usr/bin/env python

import asyncio
import websockets
import websockets.connection
from websockets.legacy.server import WebSocketServerProtocol

from package.dispatcher import Dispatcher


receiver = Dispatcher()


async def consumer_handler(websocket: WebSocketServerProtocol):
    try:
        async for message in websocket:
            await receiver.dispatch(message, websocket)
    except websockets.exceptions.ConnectionClosed as e:
        pass
    finally:
        await receiver.dispatch_disconnection(websocket)


async def main():
    async with websockets.serve(consumer_handler, "localhost", 5678):
        print("Server started on ws://localhost:5678")
        await asyncio.Future()


if __name__ == "__main__":
    asyncio.run(main())
