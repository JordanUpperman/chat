from package.user_collection import UserCollection


from websockets.legacy.server import WebSocketServerProtocol


import asyncio
import datetime
import json


class ChatHandler:
    def __init__(self, user_collection: UserCollection) -> None:
        self.__USERS = user_collection

    async def handle(
        self,
        from_user: str,
        to_user: str,
        message: str,
        from_socket: WebSocketServerProtocol,
    ):
        to_socket = self.__USERS.get_socket_for_user(to_user)
        data = {
            "type": "new_message",
            "data": {
                "ts": f"{datetime.datetime.now()}",
                "message": message,
                "from": from_user,
                "to": to_user,
            },
        }

        tasks = [to_socket.send(json.dumps(data)), from_socket.send(json.dumps(data))]
        await asyncio.gather(*tasks)
