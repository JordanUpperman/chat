from package.user_collection import UserCollection


from websockets.legacy.server import WebSocketServerProtocol


class DisconnectionHandler:
    def __init__(self, collection: UserCollection):
        self.__USERS = collection

    async def handle(self, websocket: WebSocketServerProtocol):
        user_to_remove = self.__USERS.get_user_for_socket(websocket)
        if user_to_remove:
            self.__USERS.remove_user(user_to_remove)