from websockets.legacy.server import WebSocketServerProtocol


import asyncio
from typing import Callable


class UserAlreadyExistsException(Exception):
    pass


class UserEmptyException(Exception):
    pass


class UserDoesNotExistException(Exception):
    pass


class UserCollection:
    def __init__(self):
        # TODO: Move users into redis for cross node referencing: https://redis.io/blog/how-to-create-notification-services-with-redis-websockets-and-vue-js/
        self.__COLLECTION: dict[str, WebSocketServerProtocol] = {}

    def append_user(self, usr: str, socket: WebSocketServerProtocol):
        if usr == "":
            raise UserEmptyException()
        if usr in self.__COLLECTION:
            raise UserAlreadyExistsException()
        self.__COLLECTION[usr] = socket

    def get_user_for_socket(self, websocket: WebSocketServerProtocol) -> str:
        for user, ws in self.__COLLECTION.items():
            if ws == websocket:
                return user
        raise UserDoesNotExistException()

    def get_socket_for_user(self, user: str) -> WebSocketServerProtocol:
        if user in self.__COLLECTION:
            return self.__COLLECTION[user]
        raise UserDoesNotExistException()

    def remove_user(self, usr: str):
        if usr in self.__COLLECTION:
            del self.__COLLECTION[usr]

    async def for_every_async(
        self,
        call_back: Callable[[str, WebSocketServerProtocol], None],
        max_concurrency: int = 10,
    ):
        semaphore = asyncio.Semaphore(max_concurrency)

        async def throttled_call(key: str, socket: WebSocketServerProtocol):
            async with semaphore:
                try:
                    await call_back(key, socket)
                except Exception as e:
                    print(f"Encountered error with user, {key}", e)

        tasks = [
            throttled_call(key, self.__COLLECTION[key]) for key in self.__COLLECTION
        ]
        await asyncio.gather(*tasks)
