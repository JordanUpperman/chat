from package.disconnection_handler import DisconnectionHandler
from package.chat_handler import ChatHandler
from package.registration_handler import RegistrationHandler
from package.user_collection import UserCollection


from websockets.legacy.server import WebSocketServerProtocol


import json


class Dispatcher:
    def __init__(self):
        self.__USERS = UserCollection()
        self.__registration = RegistrationHandler(self.__USERS)
        self.__chat = ChatHandler(self.__USERS)
        self.__disconnection = DisconnectionHandler(self.__USERS)

    async def dispatch(self, message: str, websocket: WebSocketServerProtocol):
        message_data = json.loads(message)
        type = message_data["type"]
        data = message_data["data"]
        if type == "register":
            await self.__registration.handle(data["screenName"], websocket)
        if type == "message":
            await self.__chat.handle(
                data["from"], data["to"], data["message"], websocket
            )

    async def dispatch_disconnection(self, websocket: WebSocketServerProtocol):
        await self.__disconnection.handle(websocket)
