from package.user_collection import UserCollection


from websockets.legacy.server import WebSocketServerProtocol


import asyncio
import json


class RegistrationHandler:
    def __init__(self, user_collection: UserCollection) -> None:
        self.__USERS = user_collection

    async def handle(self, new_user: str, new_socket: WebSocketServerProtocol):
        self.__USERS.append_user(new_user, new_socket)
        await self.__USERS.for_every_async(
            lambda user, socket: self.__user_sign_up(new_user, new_socket, user, socket)
        )

    async def __user_sign_up(
        self,
        new_user: str,
        new_socket: WebSocketServerProtocol,
        user: str,
        socket: WebSocketServerProtocol,
    ):
        if socket == new_socket:
            return
        tasks = [self.__send_user(new_user, socket), self.__send_user(user, new_socket)]
        await asyncio.gather(*tasks)

    async def __send_user(self, user: str, socket: WebSocketServerProtocol):
        await socket.send(
            json.dumps({"type": "register", "data": {"screen_name": user}})
        )
